import {Injectable} from "@angular/core";
import {Jsonp, Response} from "@angular/http";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ImageModel} from "../models/image.model";
import {Subscription} from "rxjs";

@Injectable()
export class FlickrService {

  private photos: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  private publicFeed: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

  private feedSub: Subscription;
  private requestSub: Subscription;

  constructor(private http: HttpClient, private jsonp: Jsonp) {
    this.feedSub = this.publicFeed
      .filter(feed => !!feed)
      .subscribe(feed => {
        let imageModelList = [];

        feed.items.forEach(item =>{
          imageModelList.push(new ImageModel(item));
        });

        this.photos.next(imageModelList);
      }) ;
  }

  public getPublicFeedPhotos(): Observable<any[]> {

    this.requestSub = this.jsonp.request('https://api.flickr.com/services/feeds/photos_public.gne?tags=potato&tagmode=all&format=json&jsoncallback=JSONP_CALLBACK')
      .map(this.extractData)
      .subscribe((feed) => {
        this.publicFeed.next(feed);
      });

    return this.photos;
  }

  public getPhoto(index: number): Observable<ImageModel> {
    return this.photos
      .filter(images => images.length > 0)
      .map(images => images[index]);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }

  public dispose(): void {
    if (!!this.requestSub) {
      this.requestSub.unsubscribe();
    }

    if (!!this.feedSub) {
      this.feedSub.unsubscribe();
    }
  }
}
