import {Injectable} from "@angular/core";
import * as moment from 'moment';

@Injectable()
export class UtilService {

  public formatAuthorName(str: string): string {
    let startIndex = str.indexOf('">');
    let endIndex = str.indexOf('</a>');

    return str.slice(startIndex + 2, endIndex);
  }

  public formatDate(dateString: string): string {
    return moment(dateString).format("Do MMM YYYY");
  }

  public getHoursAndMinutes(dateString : string): string {
    let date = new Date(dateString);
    let hours = date.getHours().toString();
    if (hours.length === 1) {
      hours = '0' + hours;
    }

    return hours + ':' + date.getMinutes();
  }

  public formatDescription(desc: string): string {
    let escapedText = desc.replace(/<(?:.|\n)*?>/gm, '');
    let postedAPhotoCharLength = 15;
    let startIndex = escapedText.indexOf('posted a photo:');

    return escapedText.slice(startIndex + postedAPhotoCharLength);
  }
}
