import {Component, Input} from "@angular/core";
import {ImageModel} from "../../models/image.model";

@Component({
  selector: 'photo-item',
  styleUrls: ['./photo-item.scss'],
  template: `
    <div *ngIf="photo" class="photo-list-item row">

      <div class="col-xs-3 image-wrapper">
        <img class="photo-thumbnail"
             [src]="photo.media.m"
             [routerLink]="['/photo', id]"
             alt="Thumbnail picture">
      </div>

      <div class="col-xs-9 image-data-wrapper">
        <div class="image-title"  [routerLink]="['/photo', id]">{{photo.title}}</div>


        <div class="image-details-wrapper">
          
          <span class="image-author float-left">
            <a href="https://www.flickr.com/people/{{photo.authorId}}">
              <span [innerHTML]="'<p>'+photo.author.toString()+'</p>'"></span>
            </a>
          </span>

          <span class="image-published">
            <span class="published-on">Published on: </span> {{photo.published}} at {{photo.publishedTime}}
          </span>

          <span class="image-link float-left">
            <a href="{{photo.link}}">View on Flickr</a>
          </span>

        </div>
      </div>
    </div>
  `
})
export class PhotoItemComponent {

  @Input()
  public photo: ImageModel;

  @Input()
  public id: number;

  constructor() {}
}
