import {Component} from "@angular/core";
import {Subscription} from "rxjs";
import {FlickrService} from "../../services/flickr.service";

@Component({
  selector: 'photo-list',
  styleUrls: ['./photo-list.scss'],
  template: `
    <div class="list-wrapper" *ngIf="visiblePhotos && visiblePhotos.length > 0">
      <photo-item *ngFor="let photo of visiblePhotos; let i = index"
                  [photo]="photo" [id]="i">
      </photo-item>
    </div>

    <div class="no-data-container jumbotron" *ngIf="!visiblePhotos">
      <h2>Loading data...</h2>
    </div>

    <div class="no-data-container jumbotron" *ngIf="visiblePhotos && visiblePhotos.length === 0">
      <h2>There is no data to display</h2>
    </div>
  `
})
export class PhotoListComponent {

  public visiblePhotos: any[];
  private sub: Subscription;

  constructor(private flickrService: FlickrService) {
    this.sub = flickrService.getPublicFeedPhotos()
      .filter(list => list.length !== 0)
      .subscribe((data) => {
        this.visiblePhotos = data;
      });
  }

  private getVisiblePhotos(photos: any[], lastIndex: number): any[] {
    const firstIndex = 0;
    return [...photos].splice(firstIndex, lastIndex);
  }

  ngOnDestroy() {
    if (!!this.sub) {
      this.sub.unsubscribe();
    }
  }
}
