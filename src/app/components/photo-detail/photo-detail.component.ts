import {Component} from "@angular/core";
import {ImageModel} from "../../models/image.model";
import {FlickrService} from "../../services/flickr.service";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'photo-detail',
  styleUrls: ['./photo-detail.scss'],
  template: `
    <div class="detail-wrapper" *ngIf="photo">
      <header class="photo-header">

        <div class="photo-title">
          <a href="{{photo.link}}">{{photo.title}}</a>
        </div>

        <span class="photo-author">
          <strong><a href="https://www.flickr.com/people/{{photo.authorId}}">{{photo.author}}</a></strong>
        </span>

        <span class="separator">|</span>

        <span class="photo-publish-date">
          <strong>Published:</strong> {{photo.published}} at {{photo.publishedTime}}
        </span>

        <button class="back-btn" [routerLink]="['/']">
          <i class="arrow-left"></i> Back
        </button>
      </header>

      <main class="photo-content row">
        <span class="col-md-3 image-wrapper">
            <img class="photo-thumbnail"
                 [src]="photo.media.m"
                 alt="Thumbnail picture">
        </span>

        <span class="col-md-9 image-description-wrapper">
          <span class="description">
            {{photo.description}}
          </span>
          
          <div class="photo-tags">
             <strong>Tags:</strong>
              <a href="https://www.flickr.com/photos/tags/{{tag}}" *ngFor="let tag of photo.tags">
                {{tag}}
              </a>
          </div>
        </span>

        <!--<div class="col-md-3 photo-tags"></div>-->
        <!--<div class="col-md-9 photo-tags">
          <strong>Tags:</strong>
          <a href="https://www.flickr.com/photos/tags/{{tag}}" *ngFor="let tag of photo.tags">
            {{tag}}
          </a>
        </div>-->
      </main>
    </div>
  `
})
export class PhotoDetailComponent {

  public photo: ImageModel;
  private sub: Subscription;

  constructor(private flickrSerice: FlickrService, private route: ActivatedRoute) {
    const photoId = this.route.snapshot.params['id'];

    this.sub = flickrSerice.getPhoto(photoId).subscribe(photo => {
      console.log(photo);
      this.photo = photo;
    })
  }

  ngOnDestroy() {
    if (!!this.sub) {
      this.sub.unsubscribe();
    }
  }
}
