import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { RouterModule }   from '@angular/router';

import {AppComponent} from './app.component';
import {FlickrService} from "./services/flickr.service";
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule} from "@angular/http";
import {HttpClientModule} from '@angular/common/http';
import {PhotoListComponent} from "./components/photo-list/photo-list.component";
import {PhotoItemComponent} from "./components/photo-item/photo-item.component";
import {PhotoDetailComponent} from "./components/photo-detail/photo-detail.component";

@NgModule({
  declarations: [
    AppComponent,
    PhotoListComponent,
    PhotoItemComponent,
    PhotoDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    JsonpModule,
    RouterModule.forRoot([
      {
        path: 'photo/:id',
        component: PhotoDetailComponent
      },
      {
        path: '',
        component: PhotoListComponent
      }
    ])
  ],
  providers: [FlickrService],
  bootstrap: [AppComponent]
})
export class AppModule { }
