import {Component} from '@angular/core';
import {FlickrService} from "./services/flickr.service";

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: `
    <header class="app-header">
      <span class="app-title">Flickr public feed</span>
    </header>

    <main class="main-content">
      <router-outlet></router-outlet>
    </main>
  `
})
export class AppComponent {

  constructor(private flickrService: FlickrService) {
    flickrService.getPublicFeedPhotos();
  }

  ngOnDestroy() {
    this.flickrService.dispose();
  }
}
