/*
 * author
:
"nobody@flickr.com ("Naser Ch")"
author_id
:
"62238927@N07"
date_taken
:
"2017-09-27T12:08:42-08:00"
description
:
" <p><a href="https://www.flickr.com/people/kurdphotos/">Naser Ch</a> posted a photo:</p> <p><a href="https://www.flickr.com/photos/kurdphotos/23497417518/" title="Move Over Sweet Potato, This Veggie Has Been Found To Combat Bowel Cancer"><img src="https://farm5.staticflickr.com/4485/23497417518_cd317593b9_m.jpg" width="240" height="128" alt="Move Over Sweet Potato, This Veggie Has Been Found To Combat Bowel Cancer" /></a></p> <p>... <br /> <br /> <a href="https://www.ourstyle.life/move-over-sweet-potato-this-veggie-has-been-found-to-combat-bowel-cancer/" rel="nofollow">www.ourstyle.life/move-over-sweet-potato-this-veggie-has-...</a></p>"
link
:
"https://www.flickr.com/photos/kurdphotos/23497417518/"
media
:
{m: "https://farm5.staticflickr.com/4485/23497417518_cd317593b9_m.jpg"}
published
:
"2017-09-27T10:08:42Z"
tags
:
"bowel cancer combat move potato sweet veggie"
title
:
"Move Over Sweet Potato, This Veggie Has Been Found To Combat Bowel Cancer"
 * */

import {UtilService} from "../services/util.service";

export class ImageModel {
  public author: string;
  public authorId: string;
  public dateTaken: string;
  public description: string;
  public link: string;
  public media: { m: string, };
  public published: string;
  public publishedTime: string;
  public tags: string;
  public title: string;

  constructor(image: any) {
    const utilService = new UtilService();

    this.author = utilService.formatAuthorName(image.description);
    this.authorId = image.author_id;
    this.dateTaken = image.date_taken;
    this.description = utilService.formatDescription(image.description);
    //this.description = image.description;
    this.link = image.link;
    this.media = image.media;
    this.published = utilService.formatDate(image.published);
    this.publishedTime = utilService.getHoursAndMinutes(image.published);
    //this.tags = image.tags;
    this.tags = image.tags.split(' ');
    this.title = image.title;
  }
}
